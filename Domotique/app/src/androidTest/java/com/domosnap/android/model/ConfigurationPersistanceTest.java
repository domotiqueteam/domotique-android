package com.domosnap.android.model;

import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

/**
 * Created by adegiuli on 31.08.17.
 */
@RunWith(AndroidJUnit4.class)
public class ConfigurationPersistanceTest {

    private String jsonCOnfigurationV1 ="{\"ip\":[127,0,0,1],\"login\":\"myLogin\",\"password\":\"Apassword\",\"wifiSSID\":\"32566\"}";
    private String jsonCOnfigurationV2 ="";

    @Test
    public void testLoadConfigurationFromV1(){
        Gson gson = new Gson();
        Configuration configuration = gson.fromJson(jsonCOnfigurationV1,Configuration.class);

        Assert.assertTrue(Arrays.equals(new Integer[]{127,0,0,1},configuration.getIp()));
        Assert.assertEquals("myLogin",configuration.getLogin());
        Assert.assertEquals("Apassword",configuration.getPassword());
        Assert.assertEquals("32566",configuration.getWifiSSID());

        Assert.assertEquals(Configuration.Protocole.OPEN_WEB_NET,configuration.getProtocole());

    }
}
