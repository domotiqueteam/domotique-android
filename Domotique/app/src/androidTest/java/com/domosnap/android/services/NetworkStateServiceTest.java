package com.domosnap.android.services;


import android.support.test.runner.AndroidJUnit4;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.model.GatewayCandidate;
import com.domosnap.android.model.House;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
public class NetworkStateServiceTest {

    private INetworkStateService networkStateService;

    @Before
    public void prepare(){
        IServiceManager serviceManager = DomotiqueApplication.getInstance().getServiceManager();
        networkStateService = serviceManager.getNetworkStateService();
    }

    @Test(timeout = 200_000 )
    public void testFindCandidate() throws InterruptedException {
        assertNotNull(networkStateService);
        House house = House.getInstance();
        assertNull(house.getCandidate());

        networkStateService.findCandidate();
        boolean _break = false;
        while(!_break){
            Thread.sleep(500l);
            if(house.getCandidate() !=null){
                GatewayCandidate candidate = house.getCandidate();
                assertNotNull(candidate.getProtocole());
                assertNotNull(candidate.getInetSocketAddress().getHostName());
                assertNotNull(candidate.getInetSocketAddress().getPort());
                _break = true;
            }
        }
    }
}
