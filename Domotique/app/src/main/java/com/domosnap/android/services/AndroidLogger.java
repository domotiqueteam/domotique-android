package com.domosnap.android.services;

import android.util.Log;

import com.domosnap.android.model.LogMessage;
import com.domosnap.android.model.LogMessageFactory;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;

/**
 * Created by Mikael on 21.12.2016.
 */

class AndroidLogger implements ILoggerService {

    public static final long period = 6000l;

    private ConcurrentLinkedQueue<LogMessage> logMessages = new ConcurrentLinkedQueue<>();
    private HttpLogger httpLogger = new HttpLogger();
    private TimerTask timerTask;

    Timer timer = new Timer();

    public AndroidLogger(){

         timerTask = new TimerTask() {
            @Override
            public void run() {
                ArrayList<LogMessage> messages = Lists.newArrayList();
                while(!logMessages.isEmpty()){
                    messages.add(logMessages.poll());
                }
                if(!messages.isEmpty()){
                    httpLogger.log(messages.toArray(new LogMessage[]{}));
                }
            }
        };
        timer.schedule(timerTask,0,period);

    }

    @Override
    public void log(String tag, Level level, String message){
        String sLevel = level!=null?level.getName():"TRACE";

        LogMessageFactory messageFactory = LogMessageFactory.getInstance();

        try {
            LogMessage logMessage = messageFactory.newLogMessage(tag,sLevel,message);
            logMessages.add(logMessage);
        }catch (Exception exception){
            Log.e(this.getClass().getCanonicalName(),exception.getMessage(),exception);
        }
    }

    @Override
    public void logInfo(String tag, String message) {
        Log.i(tag,message);
        log(tag,Level.INFO,message);
    }

    @Override
    public void logWarn(String tag, String message) {
        Log.w(tag,message);
        log(tag,Level.WARNING,message);
    }

    @Override
    public void logError(String tag, Throwable ex) {
        Log.e(tag,ex.getMessage(),ex);
        log(tag,Level.SEVERE,ex.getMessage());
    }

    public void shutdown(){
        if(timerTask!=null){
            timerTask.run();
        }
        timer.cancel();
        httpLogger.shutdown();
    }
}
