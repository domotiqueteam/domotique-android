package com.domosnap.android.services;

/**
 * Created by Mikael on 01/05/2016.
 */
public class Event {

    private Type type;
    private Object source;

    public Event(Type type, Object source){
        this.type = type;
        this.source = source;
    }

    public Type getType() {
        return type;
    }

    public Object getSource() {
        return source;
    }

    @Override
    public String toString() {
        return "Type "+type+" Source "+source;
    }
}
