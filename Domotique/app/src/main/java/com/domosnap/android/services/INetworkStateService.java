package com.domosnap.android.services;

/**
 * Created by Mikael on 02/05/2016.
 */
public interface INetworkStateService {
    boolean isWifiConnection();

    boolean isMobileDataConnection();

    String getWifissid();

    String getIp();

    void findCandidate();

    boolean isValide(String ip);
}
