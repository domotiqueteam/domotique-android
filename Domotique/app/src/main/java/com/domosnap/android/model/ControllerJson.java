package com.domosnap.android.model;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.controller.who.Who;

/**
 * Created by Mikael on 31.01.2017.
 */

class ControllerJson {
    private Where where;
    private String title;
    private Who who;

    public ControllerJson(Controller controller){
        this.where = controller.getWhere();
        this.title = controller.getTitle();
        this.who = controller.getWho();
    }


    public Where getWhere() {
        return where;
    }

    public String getTitle() {
        return title;
    }

    public Who getWho() {
        return who;
    }

    @Override
    public String toString() {
        return "who:" + who +" - title:"+title+" - where:"+where;
    }

    @Override
    public boolean equals(Object o) {
        if(o==null) return false;
        if(o.getClass()== ControllerJson.class ){

            return this.getWhere().equals(((ControllerJson)o).getWhere()) &&
                    this.getWho().equals(((ControllerJson)o).getWho());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.getWhere().hashCode();
    }
}
