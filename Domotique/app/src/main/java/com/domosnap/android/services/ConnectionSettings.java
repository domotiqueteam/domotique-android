package com.domosnap.android.services;

import com.domosnap.android.model.Configuration;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * immutable class
 */
final public class ConnectionSettings {
    private String ip;
    private int port;
    private String credential;
    private Configuration.Protocole protocole;

    public ConnectionSettings(String ip, int port, String password,Configuration.Protocole protocole){
        this.ip = ip;
        this.port = port;
        this.credential = password;
        this.protocole = protocole;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getCredential() {
        return credential;
    }

    public Configuration.Protocole getProtocole() {
        return protocole;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
    }
}
