package com.domosnap.android.services;

import com.domosnap.engine.connector.ControllerService;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.where.Where;

/**
 * Created by Mikael on 31.01.2017.
 */

public class ShutterService implements IShutterService{

    private ControllerService controllerService;

    @Override
    public Shutter findBy(Where where) {
        return (Shutter) controllerService.createController(Shutter.class,where);
    }


    public void setControllerService(ControllerService controllerService) {
        this.controllerService = controllerService;
    }
}
