package com.domosnap.android.services;

import java.util.concurrent.ExecutorService;

/**
 * Created by Mikael on 01/05/2016.
 */
public interface IServiceManager {

    IConnectionService getConnectionService();

    IScanService getScanService();

    INetworkStateService getNetworkStateService();

    IControllerDeviceService getControllerDeviceService();

    ILoggerService getLoggerService();

    ILightService getLightService();

    ITemperatureService getTemperatureService();

    IShutterService getAutomationService();

    ExecutorService getExecutorService();

    ExecutorService getFrontExecutorService();

    void shutdown();



}
