package com.domosnap.android.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.Button;

import com.domosnap.android.R;

/**
 * Created by Mikael on 28.11.2017.
 */

public class AbstractDialog extends Dialog {

    private Button okButton;

    public AbstractDialog(Context c, int contentView) {
        super(c);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(contentView);
        okButton = (Button) this.findViewById(R.id.ok);

    }

    public void setOkListener(Button.OnClickListener onClickListener) {
        okButton.setOnClickListener(onClickListener);
    }

}
