package com.domosnap.android.activities;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.domosnap.android.R;
import com.domosnap.android.activities.listeners.LightOnClickListener;
import com.domosnap.android.activities.listeners.ShutterDownOnClickListener;
import com.domosnap.android.activities.listeners.ShutterOnLongClickListener;
import com.domosnap.android.activities.listeners.ShutterUpOnClickListener;
import com.domosnap.android.activities.listeners.TemperatureOnLongClickListener;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.dialogs.ChangeLightLabelDialog;
import com.domosnap.android.model.House;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.what.impl.BooleanState;
import com.domosnap.engine.controller.what.impl.UpDownState;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.controller.who.Who;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by Mikael on 23.11.2016.
 */

public class DeviceAdapter extends BaseAdapter {

    private Context context;
    private House.WhereConverter whereConverter = new House.WhereConverter();
    private Map<Integer,View> controllersViews;
    private ILoggerService logger;

    private ShutterUpOnClickListener shutterUpOnClickListener;
    private ShutterDownOnClickListener shutterDownOnClickListener;
    private ShutterOnLongClickListener shutterOnLongClickListener;

    private TemperatureOnLongClickListener temperatureOnLongClickListener;

    private LightOnClickListener lightOnClickListener;
    private LightOnLongClickListener lightOnLongClickListener;


    public DeviceAdapter(Context context) {
        this.context = context;
        logger = DomotiqueApplication.getInstance().getServiceManager().getLoggerService();

        controllersViews = Maps.newHashMap();

        lightOnClickListener = new LightOnClickListener(context);
        lightOnLongClickListener = new LightOnLongClickListener();

        shutterUpOnClickListener = new ShutterUpOnClickListener(context);
        shutterDownOnClickListener = new ShutterDownOnClickListener(context);
        shutterOnLongClickListener = new ShutterOnLongClickListener(context);

        temperatureOnLongClickListener = new TemperatureOnLongClickListener(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

            Controller controller = House.getInstance().getControllers().get(position);
            View controllerView;

            controllerView = controllersViews.get(position);


            if (controllerView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (Who.LIGHT.equals(controller.getWho())) {
                    controllerView = inflater.inflate(R.layout.square_light, null);
                } else if (Who.SHUTTER.equals(controller.getWho())) {
                    controllerView = inflater.inflate(R.layout.square_shutter, null);
                } else if ((Who.TEMPERATURE_SENSOR.equals(controller.getWho()) || Who.HEATING_ADJUSTMENT.equals(controller.getWho()))) {
                    controllerView = inflater.inflate(R.layout.square_temperature, null);
                }
            }


        if (Who.LIGHT.equals(controller.getWho()) && controllerView.findViewById(R.id.lightLabel) != null) {
            refreshLightView(controllerView, (Light) controller);
        } else if (Who.SHUTTER.equals(controller.getWho()) && controllerView.findViewById(R.id.shutterLabel) != null) {
            refreshAutomationView(controllerView, (Shutter) controller);
        } else if ((Who.TEMPERATURE_SENSOR.equals(controller.getWho()) || Who.HEATING_ADJUSTMENT.equals(controller.getWho()))
                && controllerView.findViewById(R.id.temperatureLabel) != null) {
            refreshTemperatureView(controllerView, (TemperatureSensor) controller);
        }

        
            controllersViews.put(position, controllerView);


        return controllerView;

    }

    private void refreshTemperatureView(View temperatureView, TemperatureSensor temperature) {

        TextView temperatureText = (TextView) temperatureView.findViewById(R.id.temperatureLabel);
        temperatureText.setText(temperature.getTitle()!=null? temperature.getTitle() : temperature.getWhere().toString());
        TextView whereText = (TextView) temperatureView.findViewById(R.id.where);
        whereText.setText(whereConverter.toStringFormat(temperature.getWhere()));
        TextView  temperatureTextView = (TextView) temperatureView.findViewById(R.id.temperatureMeasureText);
        if (temperature.getTemperature() == null) {
            // lightImageView.setBackground(context.getResources().getDrawable(R.drawable.load));
        } else {
            // TODO add unit to enum and not hard coded as here...
            // TODO protect double value to display only on digit after ,
            temperatureTextView.setText("" + temperature.getTemperature().getDoubleValue() + " °C");
        }

        temperatureView.setOnLongClickListener(temperatureOnLongClickListener);
        temperatureView.invalidate();

    }

    private void refreshAutomationView(View automationView, Shutter automation) {

        TextView automationText = (TextView) automationView.findViewById(R.id.shutterLabel);
        automationText.setText(automation.getTitle()!=null? automation.getTitle() : automation.getWhere().toString());
        TextView whereText = (TextView) automationView.findViewById(R.id.where);
        whereText.setText(whereConverter.toStringFormat(automation.getWhere()));
        View automationUpImageView = automationView.findViewById(R.id.shutterImageUp);
        View automationDownImageView = automationView.findViewById(R.id.shutterImageDown);
        if (automation.getStatus() == null) {
            // TODO lightImageView.setBackground(context.getResources().getDrawable(R.drawable.load));
        } else if (automation.getStatus().getValue().equals(UpDownState.UpDownValue.STOP)) {
            automationUpImageView.setBackground(context.getResources().getDrawable(R.drawable.shutter_stop_up));
            automationDownImageView.setBackground(context.getResources().getDrawable(R.drawable.shutter_stop_down));
        }else if (automation.getStatus().getValue().equals(UpDownState.UpDownValue.DOWN)) {
            automationUpImageView.setBackground(context.getResources().getDrawable(R.drawable.shutter_down_up));
            automationDownImageView.setBackground(context.getResources().getDrawable(R.drawable.shutter_down_down));
            // lightImageView.setBackground(context.getResources().getDrawable(R.drawable.light_on));
        } else if (automation.getStatus().getValue().equals(UpDownState.UpDownValue.UP)) {
            automationUpImageView.setBackground(context.getResources().getDrawable(R.drawable.shutter_up_up));
            automationDownImageView.setBackground(context.getResources().getDrawable(R.drawable.shutter_up_down));
            // lightImageView.setBackground(context.getResources().getDrawable(R.drawable.light_on));
        }

        automationUpImageView.setOnClickListener(shutterUpOnClickListener);
        automationUpImageView.setOnLongClickListener(shutterOnLongClickListener);
        automationUpImageView.invalidate();

        automationDownImageView.setOnClickListener(shutterDownOnClickListener);
        automationDownImageView.setOnLongClickListener(shutterOnLongClickListener);
        automationDownImageView.invalidate();
    }

    private void refreshLightView(View lightView, Light light){
        TextView lightText = (TextView) lightView.findViewById(R.id.lightLabel);
        lightText.setText(light.getTitle()!=null? light.getTitle() : light.getWhere().toString());
        TextView whereText = (TextView) lightView.findViewById(R.id.where);
        whereText.setText(whereConverter.toStringFormat(light.getWhere()));
            View lightImageView = lightView.findViewById(R.id.lightImage);
        if (light.getStatus() == null) {
            lightImageView.setBackground(context.getResources().getDrawable(R.drawable.load));
        } else if (!isReachable(light)) {
            lightImageView.setBackground(context.getResources().getDrawable(R.drawable.light_unreachable));
        } else if (light.getStatus().getValue().equals(Boolean.FALSE)) {
            lightImageView.setBackground(context.getResources().getDrawable(R.drawable.light_off));
        }else{
            lightImageView.setBackground(context.getResources().getDrawable(R.drawable.light_on));
        }

        lightView.setOnClickListener(lightOnClickListener);
        lightView.setOnLongClickListener(lightOnLongClickListener);
        lightImageView.invalidate();
    }

    private boolean isReachable(Light light) {
        BooleanState reachable = light.isReachable();
        if (reachable != null) {
            return reachable.getValue();
        }
        return true;
    }


    public boolean hasController(Controller controller){
        if(controller==null){
            return false;
        }
        return findView(controller)!=null;
    }

    public void update(Controller controller){
        if (controller == null) {
            logger.logWarn(this.getClass().getCanonicalName(), "No controller");
            return;
        }

        logger.logInfo(this.getClass().getCanonicalName(), "Update controller " + controller.getWhere().getFrom());

        View imageView = findView(controller);

        if(imageView == null) {
            logger.logWarn(this.getClass().getCanonicalName(), "Controller View not found "+whereConverter.toStringFormat(controller.getWhere()));
            return;
        }

        if (controller instanceof Light) {
            refreshLightView(imageView, (Light) controller);
        } else if (controller instanceof Shutter) {
            refreshAutomationView(imageView, (Shutter) controller);
        } else {
            logger.logWarn(this.getClass().getCanonicalName(), "Controller not supported "+whereConverter.toStringFormat(controller.getWhere()));
        }
    }

    @Nullable
    private View findView(Controller controller) {
        View controllerImageView= null;

        for(int i=0;i<getCount();i++){
            View currentView = (View) getItem(i);

            if(currentView==null){
                logger.logWarn(this.getClass().getCanonicalName(),"null view in pos "+i);
                break;
            }

            String whereAsString = String.valueOf (((TextView)currentView.findViewById(R.id.where)).getText());
            Where where = whereConverter.convert(whereAsString);
            if(where.equals(controller.getWhere())){
                controllerImageView = currentView;
                break;
            }
        }
        return controllerImageView;
    }

    @Override
    public int getCount() {
        return House.getInstance().getControllers().size();
    }

    @Override
    public Object getItem(int position) {
       return controllersViews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class LightOnLongClickListener implements OnLongClickListener{
        @Override
        public boolean onLongClick(final View lightView) {
            final ChangeLightLabelDialog dialog = new ChangeLightLabelDialog(context);
            dialog.show();
            dialog.setOkListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                       TextView whereAsString = (TextView)lightView.findViewById(R.id.where);
                       Where where = whereConverter.convert(whereAsString.getText().toString());
                       EditText editText = (EditText) dialog.findViewById(R.id.lightNewName);
                       String newLightName = editText.getText().toString();

                       House house = House.getInstance();

                       Light light = (Light) house.findBy(where);
                       if(light!=null) {
                           light.setTitle(newLightName);
                            House.getInstance().save();
                            update(light);
                       }
                       dialog.dismiss();
                }
            });
            return true;
        }
    }

}
