package com.domosnap.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.domosnap.android.R;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.model.Configuration;
import com.domosnap.android.model.House;
import com.domosnap.android.services.ConnectionSettings;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.IConnectionService;
import com.domosnap.android.services.INetworkStateService;
import com.domosnap.android.services.IServiceManager;
import com.domosnap.android.services.Type;
import com.domosnap.engine.connector.core.ConnectionStatusEnum;

public class SplashScreenActivity extends DefaultActivity implements ViewSwitcher.ViewFactory {
    private final static int SPLASH_SCREEN_DURATION_MS = 5000;
    private final static long CHANGE_RADAR_IMG_MS = 250;

    private ImageSwitcher radarImages;

    private boolean isConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        initRadarImageSwitcher();
        initConnectionIfPossible();
    }

    private void initConnectionIfPossible() {
        Configuration configuration = Configuration.getInstance();
        IServiceManager serviceManager =  DomotiqueApplication.getInstance().getServiceManager();

        if(!configuration.isEmpty()) {
            IConnectionService connectionService = serviceManager.getConnectionService();
            String ip = configuration.getIp()[0]+"."+configuration.getIp()[1]+"."
                    +configuration.getIp()[2]+"."+configuration.getIp()[3];
            ConnectionSettings connectionSettings =
                    new ConnectionSettings(ip,
                                            configuration.getPort().intValue(),
                                            configuration.getPassword(),
                                            configuration.getProtocole());
            connectionService.connect(connectionSettings);
        }else{
            INetworkStateService networkStateService = serviceManager.getNetworkStateService();
            networkStateService.findCandidate();
        }
    }

    private void initRadarImageSwitcher() {
        radarImages = (ImageSwitcher) this.findViewById(R.id.radar);
        radarImages.setFactory(this);
        radarImages.postDelayed(new Runnable() {
            int i = 0;
            @Override
            public void run() {
                switch (i++%8){
                    case 0:
                        radarImages.setImageResource(R.drawable.radar_0);
                        break;
                    case 1:
                        radarImages.setImageResource(R.drawable.radar_1);
                        break;
                    case 2:
                        radarImages.setImageResource(R.drawable.radar_2);
                        break;
                    case 3:
                        radarImages.setImageResource(R.drawable.radar_3);
                        break;
                    case 4:
                        radarImages.setImageResource(R.drawable.radar_4);
                        break;
                    case 5:
                        radarImages.setImageResource(R.drawable.radar_5);
                        break;
                    case 6:
                        radarImages.setImageResource(R.drawable.radar_6);
                        break;
                    case 7:
                        radarImages.setImageResource(R.drawable.radar_7);
                        break;

                }
                radarImages.postDelayed(this, CHANGE_RADAR_IMG_MS);

            }
        },CHANGE_RADAR_IMG_MS);
    }

    @Override
    protected void onPerformEvent(Event event) {
        if (event.getType() == Type.CONNECTION) {
            ConnectionStatusEnum connectionStatus = (ConnectionStatusEnum) event.getSource();
            if (connectionStatus == ConnectionStatusEnum.Connected) {
                isConnect = true;
                House.getInstance().restore();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       new Handler().postDelayed(new Runnable() {
            public void run() {
                SplashScreenActivity.this.finish();

                Configuration configuration = Configuration.getInstance();

                Intent mainIntent = null;
                if(!isConnect &&  configuration.isEmpty()){
                    mainIntent = new Intent(SplashScreenActivity.this, GatewayChoiceActivity.class);
                }else if(!isConnect && configuration.getProtocole()== Configuration.Protocole.HUE){
                 mainIntent = new Intent(SplashScreenActivity.this, GatewayHueActivity.class);
                }else if(!isConnect && configuration.getProtocole()== Configuration.Protocole.OPEN_WEB_NET){
                    mainIntent = new Intent(SplashScreenActivity.this, GatewayOpenWebNetActivity.class);
                }else if(!isConnect && configuration.getProtocole()== Configuration.Protocole.KNX){
                    mainIntent = new Intent(SplashScreenActivity.this, GatewayKnxActivity.class);
                }else{
                    mainIntent = new Intent(SplashScreenActivity.this,DeviceControllerActivity.class);
                }
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                SplashScreenActivity.this.startActivity(mainIntent);
            }
        }, SPLASH_SCREEN_DURATION_MS);
    }



    @Override
    public View makeView() {
        ImageView imageView = new ImageView(this);
        return imageView;
    }
}
