package com.domosnap.android.services;

/**
 * Created by Mikael on 01/05/2016.
 */
public enum Type {
    CONNECTION, SCAN_PROGRESS, DEVICE, IP_ADDRESS_CANDIDATE, HUE_PRESS_BUTTON_REQUIRED, HUE_USER_NAME
}
