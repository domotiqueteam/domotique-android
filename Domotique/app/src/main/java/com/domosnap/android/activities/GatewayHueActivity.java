package com.domosnap.android.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.domosnap.android.R;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.dialogs.HuePressButtonDialog;
import com.domosnap.android.listeners.UiThreadProtectOnClickListener;
import com.domosnap.android.model.Configuration;
import com.domosnap.android.model.GatewayCandidate;
import com.domosnap.android.model.House;
import com.domosnap.android.services.ConnectionSettings;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.HueUserFinder;
import com.domosnap.android.services.IConnectionService;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.android.services.INetworkStateService;
import com.domosnap.android.services.IScanService;
import com.domosnap.android.services.Type;
import com.domosnap.android.utils.UIUtils;
import com.domosnap.engine.connector.core.ConnectionStatusEnum;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;

public class GatewayHueActivity extends DefaultActivity {
    private EditText ipPart1Text;
    private EditText ipPart2Text;
    private EditText ipPart3Text;
    private EditText ipPart4Text;

    private EditText portText;

    private Button connectionButton;

    private ILoggerService logger;
    private INetworkStateService networkStateService;
    private IConnectionService connectionService;
    private IScanService scanService;
    private ExecutorService frontExecutorService;
    private HueUserFinder hueUserFinder;

    private boolean alreadyDisplay;

    private HuePressButtonDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_hue_gateway);


        ipPart1Text = (EditText) this.findViewById(R.id.ipPart1);
        ipPart2Text = (EditText) this.findViewById(R.id.ipPart2);
        ipPart3Text = (EditText) this.findViewById(R.id.ipPart3);
        ipPart4Text = (EditText) this.findViewById(R.id.ipPart4);

        portText = (EditText) this.findViewById(R.id.port);

        connectionButton = (Button) this.findViewById(R.id.connect);
        connectionButton.setOnClickListener(new GatewayHueActivity.OnConnectListener());

        //for sure we set again the protocole
        Configuration.getInstance().setProtocole(Configuration.Protocole.HUE);

        networkStateService = DomotiqueApplication.getInstance().getServiceManager().getNetworkStateService();
        logger = DomotiqueApplication.getInstance().getLoggerService();
        connectionService = DomotiqueApplication.getInstance().getServiceManager().getConnectionService();
        scanService = DomotiqueApplication.getInstance().getServiceManager().getScanService();
        frontExecutorService = DomotiqueApplication.getInstance().getServiceManager().getFrontExecutorService();


    }

    @Override
    protected void onResume() {
        super.onResume();
        showGatewayCandidateProposition();
    }

    private void showGatewayCandidateProposition() {
        House house = House.getInstance();
        GatewayCandidate gatewayCandidate = house.getCandidate();
        if (gatewayCandidate != null && gatewayCandidate.getProtocole() == Configuration.Protocole.HUE) {
            String[] ip = gatewayCandidate.getInetSocketAddress().getHostName().split("\\.");

            if (ip.length == 4) {
                ipPart1Text.setText(ip[0]);
                ipPart2Text.setText(ip[1]);
                ipPart3Text.setText(ip[2]);
                ipPart4Text.setText(ip[3]);
            }

            portText.setText(gatewayCandidate.getInetSocketAddress().getPort() + "");
        }
    }

    private void saveConfiguration() {
        Configuration configuration =  Configuration.getInstance();
        Integer[] ip = new Integer[]{Integer.valueOf(ipPart1Text.getText().toString()),
                Integer.valueOf(ipPart2Text.getText().toString()),
                Integer.valueOf(ipPart3Text.getText().toString()),
                Integer.valueOf(ipPart4Text.getText().toString())
        };
        configuration.setIp(ip);
        configuration.setProtocole(Configuration.Protocole.HUE);
        int port = Integer.parseInt(portText.getText().toString());
        configuration.setPort(port);

        configuration.save();
    }

    @Override
    protected void onPerformEvent(Event event) {
        super.onPerformEvent(event);

        if (event.getType() == Type.HUE_PRESS_BUTTON_REQUIRED && !alreadyDisplay) {
            alreadyDisplay = true;
            dialog = new HuePressButtonDialog(this);
            dialog.show();
        }

        if (event.getType() == Type.HUE_USER_NAME) {
            Configuration configuration = Configuration.getInstance();
            configuration.setPassword(event.getSource().toString());
            configuration.save();
            dialog.dismiss();

            OnConnectListener onConnectListener = new OnConnectListener();
            onConnectListener.onClick(null);

        }

        if(event.getType() == Type.CONNECTION){
            Object connectionStatus = event.getSource();
            if (event.getSource() == ConnectionStatusEnum.Connected) {
                saveConfiguration();
                frontExecutorService.execute(new Runnable() {
                    public void run() {
                        scanService.scan();
                    }
                });
            } else {
                logger.logWarn(this.getClass().getCanonicalName(),"Connection fail "+connectionStatus);
                Toast.makeText(GatewayHueActivity.this.getApplicationContext(), this.getString(R.string.connection_fail) + connectionStatus,
                        Toast.LENGTH_LONG).show();
            }
        }
        if(event.getType() == Type.SCAN_PROGRESS){
            Integer progress = (Integer) event.getSource();
            if (progress >= 99) {
                Toast.makeText(GatewayHueActivity.this.getApplicationContext(), this.getString(R.string.scan_finish),
                        Toast.LENGTH_LONG).show();

                Intent gotoControllersActivityIntent = new Intent(GatewayHueActivity.this, DeviceControllerActivity.class);
                GatewayHueActivity.this.startActivity(gotoControllersActivityIntent);

            }
        }
    }

    class OnConnectListener extends UiThreadProtectOnClickListener {
        final public void _onClick(View view) {
            Activity activity = GatewayHueActivity.this;

            String ip = ipPart1Text.getText().toString() + "." + ipPart2Text.getText().toString() + "." +
                    ipPart3Text.getText().toString() + "." + ipPart4Text.getText().toString();

            if (!networkStateService.isValide(ip)) {
                logger.logInfo(this.getClass().getCanonicalName(), "BAD IP SET " + ip);
                UIUtils.showDialogInformationMessage(activity, activity.getString(R.string.bad_ip),
                        activity.getString(R.string.bad_ip_title));
                return;

            }

            int port = Integer.parseInt(portText.getText().toString());

            Configuration configuration = Configuration.getInstance();
            if (configuration.getPassword() == null ||
                    configuration.getPassword().isEmpty()) {
                GatewayCandidate gatewayCandidate = new GatewayCandidate(Configuration.Protocole.HUE
                        , new InetSocketAddress(ip, port));

                hueUserFinder = new HueUserFinder(GatewayHueActivity.this, gatewayCandidate);
                frontExecutorService.execute(hueUserFinder);
            } else {
                ConnectionSettings connectionSettings = new ConnectionSettings(ip, port, configuration.getPassword(),
                        Configuration.Protocole.HUE);
                logger.logInfo(this.getClass().getCanonicalName(), "Before connection : " + connectionSettings.toString());

                connectionService.connect(connectionSettings);
            }
        }
    }
}
