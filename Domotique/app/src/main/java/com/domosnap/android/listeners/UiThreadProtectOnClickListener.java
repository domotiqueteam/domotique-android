package com.domosnap.android.listeners;

import android.view.View;

import com.domosnap.android.application.DomotiqueApplication;

import java.util.concurrent.ExecutorService;

public abstract class UiThreadProtectOnClickListener implements View.OnClickListener {

    private ExecutorService frontExecutorService;

    public UiThreadProtectOnClickListener(){
        frontExecutorService = DomotiqueApplication.getInstance().getServiceManager().getFrontExecutorService();
    }

    @Override
    final public void onClick(final View view) {
        frontExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                UiThreadProtectOnClickListener.this._onClick(view);
            }
        });

        _afterClickOnUI(view);
    }

    public abstract void _onClick(View view);

    protected void _afterClickOnUI(View view) {
    }
}
