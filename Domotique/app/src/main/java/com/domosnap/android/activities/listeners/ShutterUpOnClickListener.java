package com.domosnap.android.activities.listeners;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.domosnap.android.R;
import com.domosnap.android.listeners.UiThreadProtectOnClickListener;
import com.domosnap.android.model.House;
import com.domosnap.android.utils.UIUtils;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.what.impl.UpDownState;

/**
 * Created by Mikael on 21.01.2018.
 */

public class ShutterUpOnClickListener extends UiThreadProtectOnClickListener {

    private final Context context;

    public ShutterUpOnClickListener(Context context) {
        this.context = context;
    }

    @Override
    public void _onClick(View shutterUpView) {
        View shutterView = (View) shutterUpView.getParent();
        TextView whereText = (TextView) shutterView.findViewById(R.id.where);

        Shutter shutter = (Shutter) House.getInstance().getControllerById(whereText.getText().toString());
        if (shutter == null) {
            UIUtils.showDialogInformationMessage(this.context,
                    context.getString(R.string.controller_not_found) + whereText.getText().toString(), "error");
            return;
        }
        if (shutter.getStatus() == null) {
            UIUtils.showDialogInformationMessage(this.context, context.getString(R.string.controller_no_status) +
                    whereText.getText().toString(), "error");
            return;
        }

        if (UpDownState.Down.equals(shutter.getStatus()) || UpDownState.Stop.equals(shutter.getStatus())) {
            shutter.setStatus(UpDownState.Up);
        } else if (UpDownState.Up.equals(shutter.getStatus())) {
            shutter.setStatus(UpDownState.Stop);
        }
    }

    @Override
    protected void _afterClickOnUI(View shutterUpView) {
        View shutterView = shutterUpView;
        shutterView.setBackground(context.getResources().getDrawable(R.drawable.load));
        shutterView.invalidate();
    }
}
