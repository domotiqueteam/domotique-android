package com.domosnap.android.services;

import com.domosnap.engine.connector.core.UnknownControllerListener;
import com.domosnap.engine.controller.ControllerChangeListener;

/**
 * Created by Mikael on 02/05/2016.
 */
public interface IControllerDeviceService extends ControllerChangeListener, UnknownControllerListener {
}
