package com.domosnap.android.services;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.model.LogMessage;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

class HttpLogger {
    public static final String URL = "http://www.domosnap.com/log.php";

    private OkHttpClient httpClient;

    private ILoggerService loggerService = DomotiqueApplication.getInstance().getLoggerService();
    private IServiceManager serviceManager;


    public HttpLogger() {
        httpClient = new OkHttpClient();
        serviceManager = DomotiqueApplication.getInstance().getServiceManager();
    }

    public void log(LogMessage[] messages) {
        Gson gson = new Gson();
        String messageAsJson = gson.toJson(messages);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), messageAsJson);



        final Request request = new Request.Builder().
                post(requestBody).
                url(URL).
                build();
        ExecutorService executor = serviceManager.getExecutorService();

        executor.submit(new Runnable() {
            @Override
            public void run() {

                try {
                    Long start = System.currentTimeMillis();
                    Response response = httpClient.newCall(request).execute();
                    response.body().close();

                    Tracker tracker = DomotiqueApplication.getInstance().getDefaultTracker();
                    tracker.send(new HitBuilders.TimingBuilder()
                            .setCategory("LOGGING")
                            .setVariable("HTTP CALL")
                            .setValue(System.currentTimeMillis()-start)
                            .build());


                    loggerService.logInfo(this.getClass().getCanonicalName(), response.body().string());
                } catch (IOException e) {
                    loggerService.logError(HttpLogger.this.getClass().getCanonicalName(),e);
                }
            }
        });



    }

    public void shutdown() {
        if(httpClient!=null && httpClient.getConnectionPool()!=null) {
            httpClient.getConnectionPool().evictAll();
            httpClient=null;
        }

    }
}

