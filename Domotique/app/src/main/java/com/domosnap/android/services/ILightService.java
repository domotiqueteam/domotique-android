package com.domosnap.android.services;

import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.where.Where;

/**
 * Created by Mikael on 31.01.2017.
 */

public interface ILightService {

    Light findBy(Where where);
}
