package com.domosnap.android.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.domosnap.android.R;
import com.domosnap.android.model.Configuration;
import com.domosnap.android.model.GatewayCandidate;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.Type;
import com.domosnap.android.utils.UIUtils;


public class GatewayChoiceActivity extends DefaultActivity {

    private View legrand;
    private View knx;
    private View hue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engine_choice);

        legrand = this.findViewById(R.id.legrandSelection);
        knx = this.findViewById(R.id.knxSelection);
        hue = this.findViewById(R.id.hueSelection);

        hue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Configuration configuration = Configuration.getInstance();
                configuration.setProtocole(Configuration.Protocole.HUE);

                Intent toHue =  new Intent(GatewayChoiceActivity.this, GatewayHueActivity.class);
                GatewayChoiceActivity.this.startActivity(toHue);

            }
        });

        knx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Configuration configuration = Configuration.getInstance();
                configuration.setProtocole(Configuration.Protocole.KNX);

                Intent toKnx =  new Intent(GatewayChoiceActivity.this, GatewayKnxActivity.class);
                GatewayChoiceActivity.this.startActivity(toKnx);

                //UIUtils.showDialogInformationMessage(GatewayChoiceActivity.this,"KNX is not Yet available, please wait for the next version !!!","Not Yet available");
            }
        });


        legrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Configuration configuration = Configuration.getInstance();
                configuration.setProtocole(Configuration.Protocole.OPEN_WEB_NET);

                Intent toLegrandConnection =  new Intent(GatewayChoiceActivity.this, GatewayOpenWebNetActivity.class);
                GatewayChoiceActivity.this.startActivity(toLegrandConnection);
            }
        });

    }

    @Override
    protected void onPerformEvent(Event event) {
        super.onPerformEvent(event);
        if (event.getType() == Type.IP_ADDRESS_CANDIDATE) {
            GatewayCandidate candidate = (GatewayCandidate) event.getSource();
            if (candidate.getProtocole() == Configuration.Protocole.OPEN_WEB_NET) {
                final Intent intent = new Intent(this, GatewayOpenWebNetActivity.class);
                UIUtils.showDialogInput(this, "Open Web", "Move to Gateway configuration", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        GatewayChoiceActivity.this.startActivity(intent);

                    }
                });
            } else if (candidate.getProtocole() == Configuration.Protocole.HUE) {
                final Intent intent = new Intent(this, GatewayHueActivity.class);
                UIUtils.showDialogInput(this, "Phillips HUE", "Move to Gateway configuration",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                GatewayChoiceActivity.this.startActivity(intent);
                            }
                        });
            }
        }
    }
}
