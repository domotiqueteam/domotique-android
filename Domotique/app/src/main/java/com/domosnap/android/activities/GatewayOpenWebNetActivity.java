package com.domosnap.android.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.domosnap.android.R;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.listeners.UiThreadProtectOnClickListener;
import com.domosnap.android.model.Configuration;
import com.domosnap.android.model.GatewayCandidate;
import com.domosnap.android.model.House;
import com.domosnap.android.services.ConnectionSettings;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.IConnectionService;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.android.services.INetworkStateService;
import com.domosnap.android.services.IScanService;
import com.domosnap.android.services.IServiceManager;
import com.domosnap.android.services.Type;
import com.domosnap.android.utils.UIUtils;
import com.domosnap.engine.connector.core.ConnectionStatusEnum;

import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.ExecutorService;

public class GatewayOpenWebNetActivity extends DefaultActivity {
    private static boolean IS_CANDIDATE_DISPLAY =false;

    private ProgressDialog connectionProgressDialog;
    private ProgressDialog scanProgressDialog;

    private Button connectionButton;

    private EditText ipPart1Text;
    private EditText ipPart2Text;
    private EditText ipPart3Text;
    private EditText ipPart4Text;

    private EditText portText;

    private EditText passwordText;

    private View gatewayInfo;

    private ILoggerService logger;
    private INetworkStateService networkStateService;

    private ExecutorService frontExecutorService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger = DomotiqueApplication.getInstance().getLoggerService();
        networkStateService = DomotiqueApplication.getInstance().getServiceManager().getNetworkStateService();
        frontExecutorService = DomotiqueApplication.getInstance().getServiceManager().getFrontExecutorService();
        setContentView(R.layout.activity_gateway);


        connectionProgressDialog = new ProgressDialog(this);
        scanProgressDialog = new ProgressDialog(this);
        scanProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        connectionButton = (Button) this.findViewById(R.id.connect);
        connectionButton.setOnClickListener(new OnConnectListener());

        ipPart1Text = (EditText) this.findViewById(R.id.ipPart1);
        ipPart2Text = (EditText) this.findViewById(R.id.ipPart2);
        ipPart3Text = (EditText) this.findViewById(R.id.ipPart3);
        ipPart4Text = (EditText) this.findViewById(R.id.ipPart4);

        portText = (EditText) this.findViewById(R.id.port);

        passwordText = (EditText) this.findViewById(R.id.gatewayPassword);

        gatewayInfo = this.findViewById(R.id.gatewayInfo);

        gatewayInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String htmlMessage = GatewayOpenWebNetActivity.this.getString(R.string.configuration_info);
                UIUtils.showDialogInformationMessage(GatewayOpenWebNetActivity.this, Html.fromHtml(htmlMessage),
                        GatewayOpenWebNetActivity.this.getString(R.string.configuration_info_title));
            }
        });

        applyConfiguration();
    }

    @Override
    protected void onResume() {
        super.onResume();
        House house = House.getInstance();
        showGatewayCandidateProposition(house.getCandidate());
    }

    @Override
    protected void onPerformEvent(Event event) {
        if (event.getType() == Type.CONNECTION) {
            connectionProgressDialog.dismiss();
            Object connectionStatus = event.getSource();
            if (event.getSource() == ConnectionStatusEnum.Connected) {
                logger.logInfo(this.getClass().getCanonicalName(),"Connection sucess ");
                Toast.makeText(GatewayOpenWebNetActivity.this.getApplicationContext(), this.getString(R.string.connection_success),
                        Toast.LENGTH_LONG).show();

                //save configuration
                saveConfiguration();

                House house = House.getInstance();

                if(house.isScanNeed()) {
                    //start scan
                    scanProgressDialog.setMessage(this.getString(R.string.scan_in_progress));
                    scanProgressDialog.onStart();
                    scanProgressDialog.show();
                    frontExecutorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            getScanService().scan();
                        }
                    });

                }else{
                    house.restore();
                    Intent gotoControllersActivityIntent = new Intent(GatewayOpenWebNetActivity.this, DeviceControllerActivity.class);
                    gotoControllersActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    gotoControllersActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    GatewayOpenWebNetActivity.this.startActivity(gotoControllersActivityIntent);
                }


            } else {
                logger.logWarn(this.getClass().getCanonicalName(),"Connection fail "+connectionStatus);
                Toast.makeText(GatewayOpenWebNetActivity.this.getApplicationContext(), this.getString(R.string.connection_fail) + connectionStatus,
                        Toast.LENGTH_LONG).show();
            }
        } else if (event.getType() == Type.SCAN_PROGRESS) {
            Integer progress = (Integer) event.getSource();
            if (progress >= 99) {
                scanProgressDialog.dismiss();
                Toast.makeText(GatewayOpenWebNetActivity.this.getApplicationContext(), this.getString(R.string.scan_finish),
                        Toast.LENGTH_LONG).show();
                //Goto controlers ac
                Intent gotoControllersActivityIntent = new Intent(GatewayOpenWebNetActivity.this, DeviceControllerActivity.class);
                GatewayOpenWebNetActivity.this.startActivity(gotoControllersActivityIntent);

            } else {
                scanProgressDialog.setProgress(progress);
            }

        } else if(event.getType()== Type.IP_ADDRESS_CANDIDATE){
            GatewayCandidate candidate = (GatewayCandidate) event.getSource();
            showGatewayCandidateProposition(candidate);
        }
    }

    private void showGatewayCandidateProposition(final GatewayCandidate candidate) {
        if (!IS_CANDIDATE_DISPLAY && candidate != null
                && candidate.getProtocole() == Configuration.Protocole.OPEN_WEB_NET) {
            String title = this.getString(R.string.ip_candidate);
            String message = String.format(this.getString(R.string.ip_candidate_body),candidate.getInetSocketAddress().getHostName());

            LayoutInflater inflater = this.getLayoutInflater();
            View view = inflater.inflate(R.layout.panel_simple_text,null);
            ((TextView)view.findViewById(R.id.simpleTextView)).setText(message);

            UIUtils.showDialogInput(this,view,title,new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    String[] ip = candidate.getInetSocketAddress().getHostName().split("\\.");

                    if(ip.length==4) {
                        ipPart1Text.setText(ip[0]);
                        ipPart2Text.setText(ip[1]);
                        ipPart3Text.setText(ip[2]);
                        ipPart4Text.setText(ip[3]);
                    }

                    portText.setText(Integer.toString(candidate.getInetSocketAddress().getPort()));

                    passwordText.requestFocus();
                }
            });
            IS_CANDIDATE_DISPLAY = true;
        }
    }

    private void applyConfiguration() {
        try{
            Configuration configuration = Configuration.getInstance();
            portText.setText(configuration.getPort()!=null ? configuration.getPort().toString() : "");
            passwordText.setText(configuration.getPassword());
            Integer[] ip =configuration.getIp();
            if(ip!=null && ip.length==4){
                ipPart1Text.setText(ip[0].toString());
                ipPart2Text.setText(ip[1].toString());
                ipPart3Text.setText(ip[2].toString());
                ipPart4Text.setText(ip[3].toString());
            }
        }catch(Exception ex){
            logger.logWarn(this.getClass().getCanonicalName(),ex.getMessage());
        }
    }

    private void saveConfiguration() {
            Configuration configuration =  Configuration.getInstance();
            configuration.setPort(Integer.valueOf(portText.getText().toString()));
            Integer[] ip = new Integer[]{Integer.valueOf(ipPart1Text.getText().toString()),
                    Integer.valueOf(ipPart2Text.getText().toString()),
                    Integer.valueOf(ipPart3Text.getText().toString()),
                    Integer.valueOf(ipPart4Text.getText().toString())
            };
            configuration.setIp(ip);
            configuration.setPassword(passwordText.getText().toString());
            configuration.setProtocole(Configuration.Protocole.OPEN_WEB_NET);
            configuration.save();
    }

    private IScanService getScanService() {
        IServiceManager serviceManager = DomotiqueApplication.getInstance().getServiceManager();
        return serviceManager.getScanService();
    }

    private IConnectionService getConnectionService() {
        IServiceManager serviceManager = DomotiqueApplication.getInstance().getServiceManager();
        return serviceManager.getConnectionService();
    }

    class OnConnectListener extends UiThreadProtectOnClickListener {
        private ConnectionSettings connectionSettings;

        final public void _onClick(View view) {
            GatewayOpenWebNetActivity activity = GatewayOpenWebNetActivity.this;

            String ip = ipPart1Text.getText().toString() + "." + ipPart2Text.getText().toString() + "." +
                    ipPart3Text.getText().toString() + "." + ipPart4Text.getText().toString();

            if (!networkStateService.isValide(ip)) {
                logger.logInfo(this.getClass().getCanonicalName(), "BAD IP SET " + ip);
                UIUtils.showDialogInformationMessage(activity, activity.getString(R.string.bad_ip),
                        activity.getString(R.string.bad_ip_title));
                return;
            }

            int port = -1;

            try {
                port = Integer.valueOf(portText.getText().toString());
            } catch (NumberFormatException nfe) {
                logger.logInfo(this.getClass().getCanonicalName(), "BAD PORT SET " + port);
                UIUtils.showDialogInformationMessage(activity, activity.getString(R.string.incorrect_port), activity.getString(R.string.incorrect_port_title));
                portText.requestFocus();
                return;
            }
            String password = passwordText.getText().toString();
            if (StringUtils.isBlank(password)) {
                logger.logInfo(this.getClass().getCanonicalName(), "No password set ");
                password = "12345";
            }


            //TODO
            //  Check the wifi up
            //  Check if the wifi match with the saved wifi
            //  Check if the inet adress is reachable
            connectionSettings = new ConnectionSettings(ip, port, password, Configuration.Protocole.OPEN_WEB_NET);
            logger.logInfo(this.getClass().getCanonicalName(), "Before connection : " + connectionSettings.toString());

        }

        @Override
        protected void _afterClickOnUI(View view) {
            if (connectionSettings != null) {
                connectionProgressDialog.setMessage(GatewayOpenWebNetActivity.this.getString(R.string.connection_in_progress));
                connectionProgressDialog.setIndeterminate(true);
                connectionProgressDialog.onStart();
                connectionProgressDialog.show();
                frontExecutorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        getConnectionService().connect(connectionSettings);

                    }
                });
            }
        }
    }
}
