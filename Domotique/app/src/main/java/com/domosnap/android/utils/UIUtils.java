package com.domosnap.android.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.domosnap.android.R;

/**
 * Created by Mikael on 23/03/2016.
 */
public final class UIUtils {

    public final static DialogInterface.OnClickListener emptyDialogInterfaceClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            // Do nothing
        }
    };

    private UIUtils(){}

    public static void showDialogInformationMessage(Context activity, CharSequence message,
                                                    String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setNegativeButton("OK",
                emptyDialogInterfaceClickListener);
        builder.setCancelable(false);
        builder.show();
    }

    public static void showDialogInput(Context activity, View view,
                                                    String title,DialogInterface.OnClickListener okListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setView(view);

        builder.setPositiveButton("OK", okListener);
        builder.setNegativeButton("Cancel",
                emptyDialogInterfaceClickListener);
        builder.setCancelable(true);

        builder.show();
    }

    public static void showDialogInput(Activity activity,
                                       String title, String message, DialogInterface.OnClickListener okListener) {


        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.panel_simple_text, null);
        ((TextView) view.findViewById(R.id.simpleTextView)).setText(message);

        showDialogInput(activity, view,
                title, okListener);


    }
}
