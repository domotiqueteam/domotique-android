package com.domosnap.android.model;

import android.os.Build;

import com.domosnap.android.BuildConfig;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.services.INetworkStateService;
import com.domosnap.android.services.IServiceManager;

/**
 * Created by Mikael on 29.01.2017.
 */

public class LogMessageFactory {

    private static LogMessageFactory instance;

    private  Integer androidVersion;
    private  Integer versionCode;
    private  String versionName;
    private  String phoneModel;
    private  String ip;
    private  String wifi;
    private String env;




    public LogMessage newLogMessage(String tag,String level, String body){
        LogMessage message = new LogMessage(tag,level,body);
        init();
        message.setAndroidVersion(this.androidVersion);
        message.setPhoneModel(this.phoneModel);
        message.setVersionCode(this.versionCode);
        message.setVersionName(this.versionName);
        message.setIp(this.ip);
        message.setWifi(this.wifi);
        message.setEnv(this.env);
        return message;
    }

    private void init(){
            IServiceManager serviceManager = DomotiqueApplication.getInstance().getServiceManager();
            INetworkStateService networkStateService = serviceManager.getNetworkStateService();

            androidVersion = android.os.Build.VERSION.SDK_INT;
            versionCode = BuildConfig.VERSION_CODE;
            versionName = BuildConfig.VERSION_NAME;
            phoneModel = Build.MODEL;
            env = DomotiqueApplication.getInstance().getEnvironment();
            wifi = networkStateService.getWifissid();
            ip =  networkStateService.getIp();
    }


    private LogMessageFactory(){
        init();
    }

    public void reInit(){
        init();
    }
    public static LogMessageFactory getInstance(){
        if(instance==null){
            instance = new LogMessageFactory();
        }
        return instance;
    }

}
