package com.domosnap.android.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.GridView;
import android.widget.ImageView;

import com.domosnap.android.R;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.model.House;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.android.services.Type;
import com.domosnap.engine.connector.ControllerService;
import com.domosnap.engine.controller.Controller;

public class DeviceControllerActivity extends DefaultActivity {

    private GridView mainGridLayout;
    private DeviceAdapter deviceAdapter;
    private ILoggerService loggerService;
    private Toolbar toolbar;
    private boolean wasConnected = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loggerService = this.getLoggerService();

        setContentView(R.layout.activity_controllers);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainGridLayout = (GridView)findViewById(R.id.gridview);
        deviceAdapter = new DeviceAdapter(this);

        mainGridLayout.setAdapter(deviceAdapter);

        updateConnectionStatus();


    }

    private void updateConnectionStatus() {
        if(toolbar==null) return;
        ImageView statusImageView = (ImageView) toolbar.findViewById(R.id.statusConnection);

        if(isConnected()) {
            statusImageView.setImageResource(R.drawable.connect);
            if(wasConnected) {
                House.getInstance().refreshControllers();
            }
            wasConnected = false;
        }
        else {
            statusImageView.setImageResource(R.drawable.not_connected);
            wasConnected = true;
        };
    }

    @Override
    protected void onPerformEvent(Event event)
    {
        if(event.getType()==Type.CONNECTION){
                updateConnectionStatus();
        }
        if(event.getType()== Type.DEVICE){
            if(event.getSource() instanceof Controller) {
                Controller controller = (Controller) event.getSource();
                if(deviceAdapter.hasController(controller)) {
                    deviceAdapter.update(controller);
                } else {
                    loggerService.logInfo(this.getClass().getCanonicalName(),"adapter recreation");
                    deviceAdapter = new DeviceAdapter(this);
                    mainGridLayout.setAdapter(deviceAdapter);
                }
                //deviceAdapter.notifyDataSetChanged();
               // deviceAdapter.notifyDataSetInvalidated();
                //mainGridLayout.invalidateViews();

            }
        }

    }

    public boolean isConnected(){
        ControllerService controllerService = DomotiqueApplication.getInstance().getControllerService();
        return controllerService.isConnected();
    }
}
