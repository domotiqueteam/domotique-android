package com.domosnap.android.activities.listeners;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.domosnap.android.R;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.dialogs.ChangeLightLabelDialog;
import com.domosnap.android.model.House;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.where.Where;


public class TemperatureOnLongClickListener implements View.OnLongClickListener {
    private final static House.WhereConverter whereConverter = new House.WhereConverter();

    private final ILoggerService logger;
    private final Context context;

    public TemperatureOnLongClickListener(Context context) {
        this.context = context;
        this.logger = DomotiqueApplication.getInstance().getServiceManager().getLoggerService();
    }

    @Override
    public boolean onLongClick(final View temperatureView) {
        final ChangeLightLabelDialog dialog = new ChangeLightLabelDialog(context);
        dialog.show();
        dialog.setOkListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView whereAsString = (TextView) temperatureView.findViewById(R.id.where);
                Where where = whereConverter.convert(whereAsString.getText().toString());
                EditText editText = (EditText) dialog.findViewById(R.id.lightNewName);
                String newTemperatureSensorName = editText.getText().toString();

                House house = House.getInstance();

                TemperatureSensor temperature = (TemperatureSensor) house.findBy(where);
                if (temperature != null) {
                    temperature.setTitle(newTemperatureSensorName);
                    House.getInstance().save();
                    if (temperature == null) {
                        logger.logWarn(this.getClass().getCanonicalName(), "No temperature sensor.");
                        return;
                    }
                    logger.logInfo(this.getClass().getCanonicalName(), "Update temperature sensor " + temperature.getWhere().getFrom());


                    // refreshAutomationView(automationImageView, automation);
                }
                dialog.dismiss();
            }
        });
        return true;
    }

}

