package com.domosnap.android.services;

import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.controller.temperature.TemperatureSensor;

/**
 * Created by Mikael on 31.01.2017.
 */

public interface ITemperatureService {

    TemperatureSensor findBy(Where where);
}
