package com.domosnap.android.model;

import java.util.Date;

public class LogMessage {
    private  Integer androidVersion;
    private  Integer versionCode;
    private  String versionName;
    private  String phoneModel;

    private String ip;
    private String wifi;
    private String env;

    private Date creation;

    private String tag;
    private String level;
    private String body;


     LogMessage(String tag,String level, String message){
        this.tag=tag;
        this.level = level;
        this.body = message;
        this.creation = new Date();
    }

     void setIp(String ip) {
        this.ip = ip;
    }

    void setAndroidVersion(Integer androidVersion) {
        this.androidVersion = androidVersion;
    }

    void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
