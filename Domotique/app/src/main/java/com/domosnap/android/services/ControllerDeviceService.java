package com.domosnap.android.services;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.engine.connector.core.Command;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.what.What;

/**
 * Created by Mikael on 02/05/2016.
 */
public class ControllerDeviceService implements IControllerDeviceService{

    private ILoggerService loggerService = DomotiqueApplication.getInstance().getLoggerService();

    @Override
    public void onStateChange(Controller controller, What what, What what1) {
        loggerService.logInfo(this.getClass().getSimpleName(),"onStateChange");
        DomotiqueApplication.getInstance().notify(new Event(Type.DEVICE,controller));
    }

    @Override
    public void onStateChangeError(Controller controller, What what, What what1) {
        loggerService.logWarn(this.getClass().getCanonicalName(),"onStateChangeError");
        DomotiqueApplication.getInstance().notify(new Event(Type.DEVICE,controller));
    }

    @Override
    public void foundUnknownController(Command command) {
/*
        TODO foundUnknownController = provide a new controller?
        House house = House.getInstance();
        house.addController(controller);
*/
    }
}
