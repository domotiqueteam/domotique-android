package com.domosnap.android.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.model.House;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.ILoggerService;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Mikael on 23/03/2016.
 */
public class DefaultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Tracker tracker = DomotiqueApplication.getInstance().getDefaultTracker();
        tracker.setScreenName(this.getClass().getSimpleName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if(isTaskRoot()){
                DomotiqueApplication.getInstance().onTerminate();
            }
        }
        return super.onKeyDown(keyCode,event);
    }

    final public void onEvent(final Event event){
        getLoggerService().logInfo(this.getClass().getCanonicalName(),event.toString());
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onPerformEvent(event);
            }
        });
    }

    protected void onPerformEvent(Event event){

    }

    final public void onModelChange(final House house){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onPerformModelChange(house);
            }
        });
    }

    protected void onPerformModelChange(House house){

    }

    protected ILoggerService getLoggerService(){
        return DomotiqueApplication.getInstance().getServiceManager().getLoggerService();
    }
}
