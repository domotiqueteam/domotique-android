package com.domosnap.android.services;

public interface IConnectionService {
    void connect(ConnectionSettings connectionSettings);
}
