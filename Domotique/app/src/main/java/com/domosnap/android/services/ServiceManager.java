package com.domosnap.android.services;

import android.util.Log;

import com.domosnap.android.application.DomotiqueApplication;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class ServiceManager implements IServiceManager{

    private ControllerDeviceService controllerDeviceService;
    private ConnectionService connectionService;
    private ScanService scanService;
    private NetworkStateService networkStateService;
    private AndroidLogger loggerService;
    private LightService lightService;
    private TemperatureService temperatureService;
    private ShutterService automationService;
    private ExecutorService executorService;
    private ExecutorService frontExecutorService;

    @Override
    public IConnectionService getConnectionService(){
        if(connectionService==null){
            connectionService = new ConnectionService();
            connectionService.setControllerService(DomotiqueApplication.getInstance().getControllerService());
        }
        return connectionService;
    }

    @Override
    public IScanService getScanService() {
        if(scanService == null){
            scanService = new ScanService();
            scanService.setControllerService(DomotiqueApplication.getInstance().getControllerService());
        }
        return scanService;
    }

    @Override
    public INetworkStateService getNetworkStateService() {
        if (networkStateService== null){
            networkStateService = new NetworkStateService();
            networkStateService.setExecutorService(this.getExecutorService());
        }
        return networkStateService ;
    }

    @Override
    public IControllerDeviceService getControllerDeviceService() {
        if(controllerDeviceService == null){
            controllerDeviceService = new ControllerDeviceService();
        }
        return controllerDeviceService;
    }

    @Override
    public ILoggerService getLoggerService() {
        if(loggerService==null){
            loggerService = new AndroidLogger();
        }
        return loggerService;
    }

    @Override
    public ILightService getLightService() {
        if(lightService==null){
            lightService = new LightService();
            lightService.setControllerService(DomotiqueApplication.getInstance().getControllerService());
        }
        return lightService;
    }

    @Override
    public ITemperatureService getTemperatureService() {
        if(temperatureService==null){
            temperatureService = new TemperatureService();
            temperatureService.setControllerService(DomotiqueApplication.getInstance().getControllerService());
        }
        return temperatureService;
    }

    @Override
    public IShutterService getAutomationService() {
        if(automationService==null){
            automationService = new ShutterService();
            automationService.setControllerService(DomotiqueApplication.getInstance().getControllerService());
        }
        return automationService;
    }

    @Override
    public ExecutorService getExecutorService() {
        if(executorService==null){
            executorService = Executors.newFixedThreadPool(10);
        }
        return executorService;

    }

    @Override
    public ExecutorService getFrontExecutorService() {
        if(frontExecutorService==null){
            frontExecutorService = Executors.newFixedThreadPool(3);
        }
        return frontExecutorService;
    }

    @Override
    public void shutdown(){
        if(loggerService!=null){
            loggerService.shutdown();
        }
        if(executorService!=null){
            executorService.shutdown();
            try {
                if(!executorService.awaitTermination(500, TimeUnit.MILLISECONDS)){
                    List<Runnable> droppedTasks =  executorService.shutdownNow();
                    Log.w(this.getClass().getCanonicalName(),"Executor did not terminate in the specified time."); //optional *
                    Log.w(this.getClass().getCanonicalName(),"Executor was abruptly shut down. " + droppedTasks.size() + " tasks will not be executed."); //optional **

                }
            } catch (InterruptedException e) {}
        }

        if(frontExecutorService!=null){
            frontExecutorService.shutdown();
        }

    }

}
