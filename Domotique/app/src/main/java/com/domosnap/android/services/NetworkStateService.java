package com.domosnap.android.services;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.model.Configuration;
import com.domosnap.android.model.GatewayCandidate;
import com.domosnap.android.model.House;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.FutureTask;

import static java.lang.Boolean.FALSE;


public class NetworkStateService implements INetworkStateService {

    private ExecutorService executorService;
    private ILoggerService loggerService = DomotiqueApplication.getInstance().getLoggerService();


    @Override
    public boolean isWifiConnection(){
        SupplicantState supState;
        WifiInfo mWifi = getWifiInfo();
        if(mWifi !=null){
            return mWifi.getNetworkId()!=-1;
        }
        return false || isTestEnvironment();
    }

    private WifiInfo getWifiInfo() {
        DomotiqueApplication application = DomotiqueApplication.getInstance();
        Activity currentActivity = application.getCurrentActivity();
        if(currentActivity !=null) {
            WifiManager wifiManager = (WifiManager) currentActivity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if(wifiManager!=null) {
                return wifiManager.getConnectionInfo();
            }
        }
        return null;
    }



    @Override
    public void findCandidate(){
        executorService.submit (new Runnable() {
            @Override
            public void run() {
                GatewayCandidate gatewayCandidate = null;
                Collection<GatewayCandidate> candidates = NetworkStateService.this.findGatewayCandidates();
                if(!candidates.isEmpty()){
                    gatewayCandidate = candidates.iterator().next();
                } else if (gatewayCandidate == null && isTestEnvironment() && isReachable("10.0.2.2", 1234)) {
                    gatewayCandidate =
                            new GatewayCandidate(Configuration.Protocole.OPEN_WEB_NET,
                                    InetSocketAddress.createUnresolved("10.0.2.2", 1234));
                } else if (gatewayCandidate == null && isTestEnvironment() &&
                        httpGet("http://10.0.2.2:8080/api/lights").contains("unauthorized user")) {
                    gatewayCandidate =
                            new GatewayCandidate(Configuration.Protocole.HUE,
                                    InetSocketAddress.createUnresolved("10.0.2.2", 8080));

                }

                if (gatewayCandidate != null) {
                    Event event = new Event(Type.IP_ADDRESS_CANDIDATE, gatewayCandidate);
                    House.getInstance().setCandidate(gatewayCandidate);
                    DomotiqueApplication.getInstance().notify(event);
                    loggerService.logWarn(this.getClass().getCanonicalName(),"IP Candidate found");
                }else {
                    loggerService.logWarn(this.getClass().getCanonicalName(), "NO IP Candidate found");
                }
            }
        });


    }

    @Override
    public boolean isValide(final String ip) {
        if(ip== null || ip.trim().isEmpty()){
            return false;
        }

        FutureTask<Boolean> correctIpFuturTask = new FutureTask<Boolean>(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                try {
                    InetAddress.getByName(ip);
                }catch (Exception ex){
                    return Boolean.FALSE;
                }
                return Boolean.TRUE;
            }
        });

        executorService.submit(correctIpFuturTask);

        try{
        for(int i=0;i<3;i++){
            if(correctIpFuturTask.isDone()){
                return correctIpFuturTask.get().booleanValue();

            }
            Thread.sleep(150l);
        }}catch (Exception ex){
            return false;
        }

        return false;
    }

    @Override
    public String getWifissid(){
        WifiInfo mWifi = getWifiInfo();
        if( mWifi !=null){
            return mWifi.getSSID();
        }
        return null;
    }

    Collection<GatewayCandidate> findGatewayCandidates() {
        if(!this.isWifiConnection()){return Collections.emptyList();}

        List<GatewayCandidate> ips = new ArrayList<>();
        Collection<FutureTask<GatewayCandidate>> futures = new ArrayList<>();
        final Boolean[] candidateFind = new Boolean[]{FALSE};

        final String subnet = "192.168.1";
        for (int i=1;i<255;i++){
            final String ip=subnet + "." + i;
            FutureTask<GatewayCandidate> ft = new FutureTask<>(new Callable<GatewayCandidate>() {
                @Override
                public GatewayCandidate call() throws Exception {
                    if(candidateFind[0]){return null;}
                    if (InetAddress.getByName(ip).isReachable(1000)){
                        if(isReachable(ip,20000)){
                            candidateFind[0] = Boolean.TRUE;
                            return new GatewayCandidate(Configuration.Protocole.OPEN_WEB_NET,
                                    InetSocketAddress.createUnresolved(ip, 20000));
                        } else if (httpGet("http://" + ip + "/api/lights").contains("unauthorized user")) {
                            return new GatewayCandidate(Configuration.Protocole.HUE,
                                    InetSocketAddress.createUnresolved(ip, 80));

                        } else if (isTestEnvironment() && isReachable(ip, 1234)) {
                            candidateFind[0] = Boolean.TRUE;
                            return new GatewayCandidate(Configuration.Protocole.OPEN_WEB_NET,
                                    InetSocketAddress.createUnresolved(ip, 1234));
                        }
                    }
                    return null;
                }
            });
            futures.add(ft);

        }

        for (FutureTask<GatewayCandidate> ft : futures) {
            executorService.execute(ft);
        }

        while(!isAllTaskDone(futures)){
            try {Thread.sleep(100l);} catch (InterruptedException e) {}

        }

        for (FutureTask<GatewayCandidate> ft : futures) {
            GatewayCandidate ip = null;
            try {
                ip= ft.get();
            }catch (Exception ex){}
            if(ip!=null){
                ips.add(ip);
            }
        }


        return ips;
    }

    private boolean isReachable(String ip, int port) {
        Socket s = null;
        try {
            s = new Socket(ip, port);
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            if( s != null){
                try {
                    s.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private boolean isAllTaskDone(Collection<FutureTask<GatewayCandidate>> futurs) {
        for(FutureTask ft : futurs){
            if(!ft.isDone()){
                return false;
            }

        }
        return true;
    }

    @Override
    public String getIp() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;
                        if (isIPv4) return sAddr;
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "0.0.0.0";
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public boolean isMobileDataConnection(){
        return false;
    }

    private boolean isTestEnvironment() {
        return DomotiqueApplication.getInstance().isTestEnvironment();
    }

    private String httpGet(String urlToRead) {
        StringBuilder result = new StringBuilder();
        BufferedReader rd = null;
        try {
            URL url = new URL(urlToRead);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
        } catch (Exception ex) {
            if (rd != null) {
                try {
                    rd.close();
                } catch (IOException e) {
                }
            }
        }
        return result.toString();
    }
}
