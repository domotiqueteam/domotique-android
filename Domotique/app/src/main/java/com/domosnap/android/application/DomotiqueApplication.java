package com.domosnap.android.application;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.domosnap.android.R;
import com.domosnap.android.activities.DefaultActivity;
import com.domosnap.android.model.Configuration;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.android.services.IServiceManager;
import com.domosnap.android.services.ServiceManager;
import com.domosnap.engine.connector.ControllerService;
import com.domosnap.engine.connector.impl.hue.HueControllerService;
import com.domosnap.engine.connector.impl.knx.KNXControllerService;
import com.domosnap.engine.connector.impl.openwebnet.OpenWebNetControllerService;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Locale;

/**
 * Created by Mikael on 30/04/2016.
 */
public class DomotiqueApplication extends Application{
    private static DomotiqueApplication instance;
    private final String environment = ENV.TEST.name();
    private ControllerService controllerService;
    private Tracker mTracker;
    private IServiceManager serviceManager;
    private ILoggerService loggerService;
    private Activity currentActivity;

    public static DomotiqueApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        loggerService = getServiceManager().getLoggerService();

        Locale local = this.getResources().getConfiguration().locale;
        loggerService.logInfo(this.getClass().getCanonicalName(),"Start Application Domotique "+local);


        this.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                currentActivity = activity;
            }

            @Override
            public void onActivityStarted(Activity activity) {
                currentActivity = activity;
            }

            @Override
            public void onActivityResumed(Activity activity) {
                currentActivity = activity;
            }

            @Override
            public void onActivityPaused(Activity activity) {
                if(activity == currentActivity) {
                    currentActivity = null;
                }
            }

            @Override
            public void onActivityStopped(Activity activity) {
                if(activity == currentActivity) {
                    currentActivity = null;
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                if(activity == currentActivity) {
                    currentActivity = null;
                }
            }
        });

        com.domosnap.engine.Log.addAppender(loggerService);

        if(Thread.getDefaultUncaughtExceptionHandler()!=null) {
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    mTracker.send(new HitBuilders.ExceptionBuilder()
                            .setDescription(ex.getMessage())
                            .setFatal(true)
                            .build());
                    DomotiqueApplication.this.loggerService.logError(this.getClass().getCanonicalName(),ex);
                }
            });
        }

    }

    public String getEnvironment() {
        return environment;
    }

    public boolean isTestEnvironment() {
        return !ENV.PROD.name().equals(environment);
    }

    @Override
    public void onTerminate() {
        loggerService.logInfo(this.getClass().getSimpleName(),"Terminate  Application Domotique");
        getServiceManager().shutdown();
        if(controllerService!=null){
            controllerService.disconnect();
        }


        if(getCurrentActivity()!=null){
            getCurrentActivity().finishAffinity();
        }
        super.onTerminate();
        System.exit(0);
    }

    public ControllerService getControllerService(){

        if(controllerService!=null) {
            // TODO when we will manage multiprotocole this probably will be remove...
            // For the moment need when we go on GatewayOpenWebNetActivity and come back
            // to GatewayHueActivity => service controller have been instanciate and a class cast
            // occurs...
            Configuration configuration = Configuration.getInstance();
            if (controllerService instanceof OpenWebNetControllerService && configuration.getProtocole()!=Configuration.Protocole.OPEN_WEB_NET) {
                controllerService = null; // protocole and service controller different = reset
            }

            if (controllerService instanceof HueControllerService && configuration.getProtocole()!=Configuration.Protocole.HUE) {
                controllerService = null; // protocole and service controller different = reset
            }


            if (controllerService instanceof KNXControllerService && configuration.getProtocole()!=Configuration.Protocole.KNX) {
                controllerService = null; // protocole and service controller different = reset
            }
        }

        if(controllerService==null){
            Configuration configuration = Configuration.getInstance();
            if(configuration.getProtocole() == Configuration.Protocole.HUE){
                controllerService = new HueControllerService();
            }else if(configuration.getProtocole() == Configuration.Protocole.OPEN_WEB_NET) {
                controllerService = new OpenWebNetControllerService();
            } else {
                controllerService = new KNXControllerService();
            }
        }
        return controllerService;
    }

    public IServiceManager getServiceManager(){
        if(serviceManager == null){
            serviceManager = new ServiceManager();
        }
        return serviceManager;
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void notify(Event event){
        if(getCurrentActivity() instanceof DefaultActivity){
            ((DefaultActivity)getCurrentActivity()).onEvent(event);
        }

    }

    public ILoggerService getLoggerService() {
        return loggerService;
    }

    private enum ENV {
        TEST,
        PROD
    }
}
