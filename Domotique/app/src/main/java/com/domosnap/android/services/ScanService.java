package com.domosnap.android.services;

import com.domosnap.android.activities.DefaultActivity;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.model.House;
import com.domosnap.engine.connector.ControllerService;
import com.domosnap.engine.connector.core.ScanListener;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.controller.who.Who;
/**
 * Created by Mikael on 01/05/2016.
 */
public class ScanService implements IScanService, ScanListener {

    private ControllerService controllerService;
    private ILoggerService loggerService = DomotiqueApplication.getInstance().getLoggerService();

    @Override
    public void scan() {
        controllerService.scan(this);
    }

    public void setControllerService(ControllerService controllerService) {
        this.controllerService = controllerService;
    }

    @Override
    public void foundController(Who who, Where where, Controller controller) {
        if (controller != null) {
            loggerService.logInfo(this.getClass().getCanonicalName(), "foundController Who : " + who + " Where : " + where + " Controller : " + controller.getClass().getSimpleName());
            House house = House.getInstance();
            house.addController(controller);

            controller.addControllerChangeListener(DomotiqueApplication.getInstance().getServiceManager().getControllerDeviceService());

            if (DomotiqueApplication.getInstance().getCurrentActivity() instanceof DefaultActivity) {
                DefaultActivity currentActivity = (DefaultActivity) DomotiqueApplication.getInstance().getCurrentActivity();
                currentActivity.onModelChange(house);
            }
        } else {
            loggerService.logWarn(this.getClass().getCanonicalName(), " Controller is null Who : " + who + " Where : " + where);
        }
    }

    @Override
    public void progess(int i) {
        DomotiqueApplication.getInstance().notify(new Event(Type.SCAN_PROGRESS,Integer.valueOf(i)));
    }

    @Override
    public void scanFinished() {
        House house = House.getInstance();
        loggerService.logInfo(this.getClass().getCanonicalName(),"scan finish with "+house.getControllers().size());
        house.save();

    }
}
