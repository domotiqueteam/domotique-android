package com.domosnap.android.activities.listeners;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.domosnap.android.R;
import com.domosnap.android.listeners.UiThreadProtectOnClickListener;
import com.domosnap.android.model.House;
import com.domosnap.android.utils.UIUtils;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.what.impl.UpDownState;

/**
 * Created by Mikael on 21.01.2018.
 */

public class ShutterDownOnClickListener extends UiThreadProtectOnClickListener {

    private final Context context;

    public ShutterDownOnClickListener(Context context) {
        this.context = context;
    }

    @Override
    public void _onClick(View shutterDownView) {
        View shutterView = (View) shutterDownView.getParent();
        TextView whereText = (TextView) shutterView.findViewById(R.id.where);

        Shutter currentController = (Shutter) House.getInstance().getControllerById(whereText.getText().toString());
        if (currentController == null) {
            UIUtils.showDialogInformationMessage(context,
                    context.getString(R.string.controller_not_found) + whereText.getText().toString(), "error");
            return;
        }
        if (currentController.getStatus() == null) {
            UIUtils.showDialogInformationMessage(context, context.getString(R.string.controller_no_status) +
                    whereText.getText().toString(), "error");
            return;
        }

        if (UpDownState.Up.equals(currentController.getStatus()) || UpDownState.Stop.equals(currentController.getStatus())) {
            currentController.setStatus(UpDownState.Down);
        } else if (UpDownState.Down.equals(currentController.getStatus())) {
            currentController.setStatus(UpDownState.Stop);
        }
    }

    @Override
    protected void _afterClickOnUI(View shutterDownView) {
        View shutterView = shutterDownView;
        shutterView.setBackground(context.getResources().getDrawable(R.drawable.load));
        shutterView.invalidate();
    }
}
