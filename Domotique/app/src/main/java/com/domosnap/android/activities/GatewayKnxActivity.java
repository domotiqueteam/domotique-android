package com.domosnap.android.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.domosnap.android.R;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.listeners.UiThreadProtectOnClickListener;
import com.domosnap.android.model.Configuration;
import com.domosnap.android.model.GatewayCandidate;
import com.domosnap.android.model.House;
import com.domosnap.android.services.ConnectionSettings;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.IConnectionService;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.android.services.IScanService;
import com.domosnap.android.services.Type;
import com.domosnap.engine.connector.core.ConnectionStatusEnum;

import java.util.concurrent.ExecutorService;

public class GatewayKnxActivity extends DefaultActivity {


    private Button connectionButton;

    private ILoggerService logger;
    private IConnectionService connectionService;
    private IScanService scanService;
    private ExecutorService frontExecutorService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_knx_gateway);

        connectionButton = (Button) this.findViewById(R.id.connect);
        connectionButton.setOnClickListener(new GatewayKnxActivity.OnConnectListener());

        //for sure we set again the protocole
        Configuration.getInstance().setProtocole(Configuration.Protocole.KNX);

        logger = DomotiqueApplication.getInstance().getLoggerService();
        connectionService = DomotiqueApplication.getInstance().getServiceManager().getConnectionService();
        scanService = DomotiqueApplication.getInstance().getServiceManager().getScanService();
        frontExecutorService = DomotiqueApplication.getInstance().getServiceManager().getFrontExecutorService();


    }

    @Override
    protected void onResume() {
        super.onResume();
        showGatewayCandidateProposition();
    }

    private void showGatewayCandidateProposition() {
        House house = House.getInstance();
        GatewayCandidate gatewayCandidate = house.getCandidate();

    }

    private void saveConfiguration() {
        Configuration configuration =  Configuration.getInstance();
        configuration.setProtocole(Configuration.Protocole.KNX);
        configuration.save();
    }

    @Override
    protected void onPerformEvent(Event event) {
        super.onPerformEvent(event);

        if(event.getType() == Type.CONNECTION){
            Object connectionStatus = event.getSource();
            if (event.getSource() == ConnectionStatusEnum.Connected) {
                saveConfiguration();
                frontExecutorService.execute(new Runnable() {
                    public void run() {
                        scanService.scan();
                    }
                });
            } else {
                logger.logWarn(this.getClass().getCanonicalName(),"Connection fail "+connectionStatus);
                Toast.makeText(GatewayKnxActivity.this.getApplicationContext(), this.getString(R.string.connection_fail) + connectionStatus,
                        Toast.LENGTH_LONG).show();
            }
        }
        if(event.getType() == Type.SCAN_PROGRESS){
            Integer progress = (Integer) event.getSource();
            if (progress >= 99) {
                Toast.makeText(GatewayKnxActivity.this.getApplicationContext(), this.getString(R.string.scan_finish),
                        Toast.LENGTH_LONG).show();

                Intent gotoControllersActivityIntent = new Intent(GatewayKnxActivity.this, DeviceControllerActivity.class);
                GatewayKnxActivity.this.startActivity(gotoControllersActivityIntent);

            }
        }
    }

    class OnConnectListener extends UiThreadProtectOnClickListener {
        final public void _onClick(View view) {
            Activity activity = GatewayKnxActivity.this;

            ConnectionSettings connectionSettings = new ConnectionSettings(null, 0, null,
                      Configuration.Protocole.KNX);

            logger.logInfo(this.getClass().getCanonicalName(), "Before connection : " + connectionSettings.toString());

            connectionService.connect(connectionSettings);
        }
    }
}
