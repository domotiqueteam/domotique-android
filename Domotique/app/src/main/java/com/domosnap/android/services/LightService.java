package com.domosnap.android.services;

import com.domosnap.engine.connector.ControllerService;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.where.Where;

/**
 * Created by Mikael on 31.01.2017.
 */

public class LightService implements ILightService {

    private ControllerService controllerService;

    @Override
    public Light findBy(Where where) {
        return (Light) controllerService.createController(Light.class,where);
    }


    public void setControllerService(ControllerService controllerService) {
        this.controllerService = controllerService;
    }
}
