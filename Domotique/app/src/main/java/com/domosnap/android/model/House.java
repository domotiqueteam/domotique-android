package com.domosnap.android.model;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.VisibleForTesting;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.services.Event;
import com.domosnap.android.services.ILightService;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.android.services.IServiceManager;
import com.domosnap.android.services.IShutterService;
import com.domosnap.android.services.ITemperatureService;
import com.domosnap.android.services.Type;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.controller.who.Who;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

final public class House {

    private final static String HOUSE_KEY = "House.Key";
    private static House house = null;

    private ILoggerService loggerService;
    private Map<Where, Controller> controllers = new ConcurrentHashMap<>();
    // private ControllerJson[] contollersJsons;
    private WhereConverter whereConverter = new WhereConverter();
    private GatewayCandidate candidate;

    @VisibleForTesting
    protected House() {
        loggerService = DomotiqueApplication.getInstance().getLoggerService();
    }

    public static House getInstance() {
        if (house == null) {
            house = new House();
        }
        return house;
    }

    public boolean isScanNeed() {
        return (controllers.size() == 0);
    }


    public void refreshControllers() {
        for (Controller controller : getControllers()) {
            controller.requestAllStatus();
        }
    }

    public List<Controller> getControllers() {
        List<Controller> controllers = new ArrayList<>();
        for (Controller controller : this.controllers.values()) {
            controllers.add(controller);
        }
        Collections.sort(controllers, new Comparator<Controller>() {
            @Override
            public int compare(Controller lhs, Controller rhs) {
                return lhs.getWhere().getFrom().compareTo(rhs.getWhere().getFrom());
            }
        });
        return controllers;
    }

    public void save() {
        List<Controller> controllers = getControllers();
        ControllerJson[] newlightsJsons = new ControllerJson[controllers.size()];
        for (int i = 0; i < controllers.size(); i++) {
            newlightsJsons[i] = new ControllerJson(controllers.get(i));
        }

//        newlightsJsons = protectAgainLoosingLight(newlightsJsons);

        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Gson gson = new Gson();
        String lightsJsonAsString = gson.toJson(newlightsJsons);
        loggerService.logInfo(this.getClass().getCanonicalName(), "save : " + lightsJsonAsString);

        editor.putString(HOUSE_KEY, lightsJsonAsString);
        editor.commit();
//        this.contollersJsons = newlightsJsons;

    }

//    private ControllerJson[] protectAgainLoosingLight(ControllerJson[] newLightsJsons) {
//        List<ControllerJson> loosingLights = new ArrayList<>();
//
//        if(this.contollersJsons !=null){
//            for(ControllerJson lightJson : this.contollersJsons){
//                List<ControllerJson> newLightToBeSaved = Arrays.asList(newLightsJsons);
//                if(!newLightToBeSaved.contains(lightJson)){
//                    loosingLights.add(lightJson);
//                }
//            }
//        }
//        if(!loosingLights.isEmpty()){
//            loggerService.logWarn(this.getClass().getCanonicalName(),"Some light was saved "+loosingLights);
//            return ArrayUtils.addAll(newLightsJsons,loosingLights.toArray(new LightJson[]{}));
//        }
//        return newLightsJsons;
//    }

    public void restore() {
        if (controllers.size() == 0) {
//            if (contollersJsons != null) {
            final IServiceManager serviceManager = DomotiqueApplication.getInstance().getServiceManager();
            final ILightService lightService = serviceManager.getLightService();
            final IShutterService automationService = serviceManager.getAutomationService();
            final ITemperatureService temperatureService = serviceManager.getTemperatureService();
            final ControllerJson[] controllerJsonsList = read();

            if (controllerJsonsList == null) {
                return;
            }

            ExecutorService executor = serviceManager.getExecutorService();
            executor.submit(
                    new Runnable() {
                        @Override
                        public void run() {
                            for (final ControllerJson c : controllerJsonsList) {

                                //Light light = lightService.findBy(lightJson.getWhere();
                                Controller cc = null;
                                if (Who.LIGHT.equals(c.getWho())) {
                                    cc = lightService.findBy(c.getWhere());
                                } else if (Who.SHUTTER.equals(c.getWho())) {
                                    cc = automationService.findBy(c.getWhere());
                                } else if (Who.TEMPERATURE_SENSOR.equals(c.getWho()) || Who.HEATING_ADJUSTMENT.equals(c.getWho())) {
                                    cc = temperatureService.findBy(c.getWhere());
                                }

                                if (cc != null) {
                                    cc.addControllerChangeListener(serviceManager.getControllerDeviceService());

                                    //House.this.addController(light);
                                    controllers.put(cc.getWhere(), cc);
                                    Event event = new Event(Type.DEVICE, cc);
                                    DomotiqueApplication.getInstance().notify(event);
                                } else {
                                    // log unknown controller
                                }
                            }
                        }
                    }
            );
//            }
        }
    }


    private ControllerJson[] read() {

        ControllerJson[] contollersJsons = null;

        SharedPreferences sharedPreferences = getSharedPreferences();
        if (sharedPreferences == null) {
            return new ControllerJson[0];
        }

        String lightsJsonAsString = sharedPreferences.getString(HOUSE_KEY, null);
        if (lightsJsonAsString != null) {
            Gson gson = new Gson();
            contollersJsons = gson.fromJson(lightsJsonAsString, ControllerJson[].class);
            loggerService.logInfo(this.getClass().getCanonicalName(), "restore : " + lightsJsonAsString);

        }

        return contollersJsons;
    }


    public void addController(Controller controller) {
        Preconditions.checkNotNull(controller);
        Preconditions.checkNotNull(controller.getWhere(), "no where information for the controller" + controller);

        if (!controllers.containsKey(controller.getWhere())) {
            controllers.put(controller.getWhere(), controller);
            this.save();
        }
    }

//    private boolean isNotSave(Where where) {
//        if(contollersJsons !=null){
//            for(LightJson ls : this.contollersJsons){
//                if(where.equals(ls.getWhere())){
//                    return false;
//                }
//            }
//        }
//        return true;
//    }

    public Controller getControllerById(String s) {
        Where where = whereConverter.convert(s);
        return controllers.get(where);
    }

    public Controller findBy(Where where) {
        return controllers.get(where);
        //return null;
    }

    private SharedPreferences getSharedPreferences() {
        try {
            Activity context = DomotiqueApplication.getInstance().getCurrentActivity();
            if (context != null) {
                return context.getSharedPreferences("HOUSE", Context.MODE_PRIVATE);
            }
        } catch (Exception e) {
            loggerService.logWarn(this.getClass().getCanonicalName(), e.getMessage());
        }
        return null;
    }

    public GatewayCandidate getCandidate() {
        return candidate;
    }

    public void setCandidate(GatewayCandidate candidate) {
        this.candidate = candidate;
    }

    public static class WhereConverter {
        private static String separator = "|";

        public String toStringFormat(Where where) {
            if (where != null) {
                return where.getFrom() + separator + where.getTo();
            } else {
                return "no where";
            }
        }

        public Where convert(String whereFormat) {
            String[] wheres = whereFormat.split("\\|");
            if (wheres.length != 2) {
                //TODO
            }
            Where where = new Where(wheres[0], wheres[1]);
            return where;
        }
    }
}
