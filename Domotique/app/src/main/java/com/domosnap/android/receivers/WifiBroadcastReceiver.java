package com.domosnap.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.services.ILoggerService;

/**
 * Created by Mikael on 02/05/2016.
 */
public class WifiBroadcastReceiver extends BroadcastReceiver {

    private ILoggerService loggerService = DomotiqueApplication.getInstance().getLoggerService();

    @Override
    public void onReceive(Context context, Intent intent) {
        loggerService.logInfo(this.getClass().getCanonicalName(),intent.getAction());
        //DomotiqueApplication.getInstance().notify();
    }
}
