package com.domosnap.android.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.domosnap.android.R;


public class HuePressButtonDialog extends AbstractDialog {


    public HuePressButtonDialog(@NonNull Context context) {
        super(context, R.layout.dialog_hue_connect);
        setOkListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HuePressButtonDialog.this.dismiss();
            }
        });
    }

}
