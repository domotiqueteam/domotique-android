package com.domosnap.android.model;

import java.net.InetSocketAddress;


public class GatewayCandidate {
    private final Configuration.Protocole protocole;
    private final InetSocketAddress inetSocketAddress;

    public GatewayCandidate(Configuration.Protocole protocole, InetSocketAddress inetSocketAddress) {
        this.protocole = protocole;
        this.inetSocketAddress = inetSocketAddress;
    }

    public Configuration.Protocole getProtocole() {
        return protocole;
    }

    public InetSocketAddress getInetSocketAddress() {
        return inetSocketAddress;
    }
}
