package com.domosnap.android.services;

import com.domosnap.engine.Appender;

public interface ILoggerService extends Appender {

    void logInfo(String tag,String message);
    void logWarn(String tag,String message);
    void logError(String tag,Throwable ex);

}
