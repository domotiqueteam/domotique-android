package com.domosnap.android.model;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.services.ILoggerService;
import com.google.common.annotations.VisibleForTesting;
import com.google.gson.Gson;

public class Configuration {
    private final static String CONFIGURATION_KEY = "PreferenceService.Configuration.Key";
    private static ILoggerService loggerService = DomotiqueApplication.getInstance().getLoggerService();
    private static Configuration instance;
    private String password;
    private String login;
    private Integer[] ip;
    private Integer port;
    private String wifiSSID;
    private Protocole protocole;

    @VisibleForTesting
    protected Configuration() {
    }

    public static Configuration  getInstance(){
        if(instance==null){
            {
                SharedPreferences sharedPreferences = getSharedPreferences();
                String configurationAsJson = sharedPreferences.getString(CONFIGURATION_KEY,null);
                if(configurationAsJson == null){
                    instance =  new Configuration();
                } else {
                    try {
                        Gson gson = new Gson();
                        instance = gson.fromJson(configurationAsJson, Configuration.class);
                    } catch (Exception e) {
                        loggerService.logWarn(Configuration.class.getCanonicalName(), e.getMessage());
                        instance = new Configuration();
                    }
                }
            }
        }
        return instance;
    }

    private static SharedPreferences getSharedPreferences() {
        try {
            Activity context = DomotiqueApplication.getInstance().getCurrentActivity();
            return context.getSharedPreferences("PREFERENCES", Context.MODE_PRIVATE);
        } catch (Exception e) {
            loggerService.logWarn(Configuration.class.getCanonicalName(), e.getMessage());
        }
        return null;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer[] getIp() {
        return ip;
    }

    public void setIp(Integer[] ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getWifiSSID() {
        return wifiSSID;
    }

    public void setWifiSSID(String wifiSSID) {
        this.wifiSSID = wifiSSID;
    }

    public boolean isEmpty(){
        return ip==null;
    }

    public Protocole getProtocole() {
        if(protocole==null){
            return Protocole.OPEN_WEB_NET;
        }
        return protocole;
    }

    public void setProtocole(Protocole protocole) {
        this.protocole = protocole;
    }

    public void save(){
        SharedPreferences sharedPreferences = getSharedPreferences();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson= new Gson();
        String configurationAsJson = gson.toJson(this);
        editor.putString(CONFIGURATION_KEY,configurationAsJson);
        editor.commit();
    }

    public enum Protocole {
        OPEN_WEB_NET,
        HUE,
        KNX
    }
}
