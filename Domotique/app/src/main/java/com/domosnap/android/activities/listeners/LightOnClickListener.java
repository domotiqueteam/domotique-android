package com.domosnap.android.activities.listeners;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.domosnap.android.R;
import com.domosnap.android.listeners.UiThreadProtectOnClickListener;
import com.domosnap.android.model.House;
import com.domosnap.android.utils.UIUtils;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.impl.OnOffState;

/**
 * Created by Mikael on 21.01.2018.
 */

public class LightOnClickListener extends UiThreadProtectOnClickListener {

    private final Context context;

    public LightOnClickListener(Context context) {
        this.context = context;
    }


    @Override
    public void _onClick(View currentLightView) {
        TextView whereText = (TextView) currentLightView.findViewById(R.id.where);

        Light currentController = (Light) House.getInstance().getControllerById(whereText.getText().toString());
        if (currentController == null) {
            UIUtils.showDialogInformationMessage(context,
                    context.getString(R.string.controller_not_found) + whereText.getText().toString(), "error");
            return;
        }
        if (currentController.getStatus() == null) {
            UIUtils.showDialogInformationMessage(context, context.getString(R.string.controller_no_status) +
                    whereText.getText().toString(), "error");
            return;
        }

        if (currentController.getStatus().getValue()) {
            currentController.setStatus(OnOffState.Off());
        } else {
            currentController.setStatus(OnOffState.On());
        }
    }

    @Override
    protected void _afterClickOnUI(View currentLightView) {
        View lightImageView = currentLightView.findViewById(R.id.lightImage);
        lightImageView.setBackground(context.getResources().getDrawable(R.drawable.load));
        lightImageView.invalidate();
    }
}
