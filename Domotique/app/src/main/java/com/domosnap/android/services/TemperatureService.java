package com.domosnap.android.services;

import com.domosnap.engine.connector.ControllerService;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.controller.temperature.TemperatureSensor;

/**
 * Created by Mikael on 31.01.2017.
 */

public class TemperatureService implements ITemperatureService {

    private ControllerService controllerService;

    @Override
    public TemperatureSensor findBy(Where where) {
        return (TemperatureSensor) controllerService.createController(TemperatureSensor.class,where);
    }


    public void setControllerService(ControllerService controllerService) {
        this.controllerService = controllerService;
    }
}
