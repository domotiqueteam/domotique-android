package com.domosnap.android.activities.listeners;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.domosnap.android.R;
import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.android.dialogs.ChangeLightLabelDialog;
import com.domosnap.android.model.House;
import com.domosnap.android.services.ILoggerService;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.where.Where;


public class ShutterOnLongClickListener implements View.OnLongClickListener {
    private final static House.WhereConverter whereConverter = new House.WhereConverter();

    private final ILoggerService logger;
    private final Context context;

    public ShutterOnLongClickListener(Context context) {
        this.context = context;
        this.logger = DomotiqueApplication.getInstance().getServiceManager().getLoggerService();
    }

    @Override
    public boolean onLongClick(final View shutterView) {
        final ChangeLightLabelDialog dialog = new ChangeLightLabelDialog(context);
        dialog.show();
        dialog.setOkListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView whereAsString = (TextView) shutterView.findViewById(R.id.where);
                Where where = whereConverter.convert(whereAsString.getText().toString());
                EditText editText = (EditText) dialog.findViewById(R.id.lightNewName);
                String newLightName = editText.getText().toString();

                House house = House.getInstance();

                Shutter shutter = (Shutter) house.findBy(where);
                if (shutter != null) {
                    shutter.setTitle(newLightName);
                    House.getInstance().save();
                    if (shutter == null) {
                        logger.logWarn(this.getClass().getCanonicalName(), "No Shutter");
                        return;
                    }
                    logger.logInfo(this.getClass().getCanonicalName(), "Update Shutter " + shutter.getWhere().getFrom());


                }
                dialog.dismiss();
            }
        });
        return true;
    }

}

