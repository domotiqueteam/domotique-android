package com.domosnap.android.services;

import com.domosnap.android.application.DomotiqueApplication;
import com.domosnap.engine.connector.ControllerService;
import com.domosnap.engine.connector.core.ConnectionListener;
import com.domosnap.engine.connector.core.ConnectionStatusEnum;
import com.domosnap.engine.connector.impl.hue.HueControllerService;
import com.domosnap.engine.connector.impl.openwebnet.OpenWebNetControllerService;


/**
 * Created by Mikael on 01/05/2016.
 */
public class ConnectionService implements IConnectionService, ConnectionListener {

    private ControllerService controllerService;
    private ILoggerService loggerService = DomotiqueApplication.getInstance().getLoggerService();

    @Override
    public void connect(ConnectionSettings connectionSettings) {

        switch (connectionSettings.getProtocole()){
            case OPEN_WEB_NET:
                OpenWebNetControllerService openWebNetControllerService = (OpenWebNetControllerService) controllerService;
                openWebNetControllerService.setIp(connectionSettings.getIp());
                openWebNetControllerService.setPort(connectionSettings.getPort());
                openWebNetControllerService.setPassword(Integer.valueOf(connectionSettings.getCredential()));
                break;

            case HUE:
                HueControllerService hueControllerService = (HueControllerService) controllerService;
                hueControllerService.setIp(connectionSettings.getIp());
                hueControllerService.setPort(connectionSettings.getPort());
                hueControllerService.setUser(connectionSettings.getCredential());
                break;

            case KNX:
                //KNXControllerService knxControllerService = (KNXControllerService) controllerService;
                break;
            default:
                throw new RuntimeException("Unmanage Controller service  "+ controllerService.getClass().getSimpleName());

        }

        controllerService.connect();
    }

    protected void setControllerService(ControllerService controllerService) {
        this.controllerService = controllerService;

        this.controllerService.addCommanderConnectionListener(this);
        this.controllerService.addMonitorConnectionListener(this);
    }

    @Override
    public void onClose() {
        DomotiqueApplication.getInstance().notify(new Event(Type.CONNECTION,"close"));
        loggerService.logInfo(this.getClass().getCanonicalName(),"onClose");
    }

    @Override
    public void onConnect(final ConnectionStatusEnum connectionStatusEnum) {
        DomotiqueApplication.getInstance().notify(new Event(Type.CONNECTION,connectionStatusEnum));
    }
}
