package com.domosnap.android.services;

import com.domosnap.android.activities.DefaultActivity;
import com.domosnap.android.model.GatewayCandidate;
import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;


public class HueUserFinder implements Runnable {

    private final OkHttpClient okHttpClient = new OkHttpClient();
    private final GatewayCandidate gatewayCandidate;
    private final DefaultActivity activity;
    private final Gson gson;


    public HueUserFinder(DefaultActivity activity, GatewayCandidate gatewayCandidate) {
        this.gatewayCandidate = gatewayCandidate;
        this.activity = activity;
        this.gson = new Gson();
    }

    @Override
    public void run() {
        String username = null;
        String url = "http://" + gatewayCandidate.getInetSocketAddress().getHostName() + ":"
                + gatewayCandidate.getInetSocketAddress().getPort() + "/api";

        final Request request = new Request.Builder().
                post(RequestBody.create(MediaType.parse("application/json"),
                        "{\"devicetype\":\"domosnap_app#android\"}")).
                url(url).
                build();

        while (true) {
            try {
                Response response = okHttpClient.newCall(request).execute();

                String jsonResponse = new String(response.body().bytes());

                if (jsonResponse == null || jsonResponse.contains("error")) {
                    activity.onEvent(new Event(Type.HUE_PRESS_BUTTON_REQUIRED, ""));
                }
                if (jsonResponse.contains("success")) {
                    Success[] successes = gson.fromJson(jsonResponse, Success[].class);
                    if (successes.length == 1) {
                        username = successes[0].success.username;
                        activity.onEvent(new Event(Type.HUE_USER_NAME, username));
                        break;
                    }
                }


            } catch (IOException e) {
                sleep(10000l);
            }

            sleep(2000l);
        }

    }

    private void sleep(long duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
        }
    }


    /**
     *
     [
     {
     "error": {
     "type": 101,
     "address": "",
     "description": "link button not pressed"
     }
     }
     ]


     [
     {
     "success": {
     "username": "JoFu665Wu4--yCocSA3H1iUILO3cLm7dXTDaDj--"
     }
     }
     ]

     *
     * **/
}

class Success {
    User success;
}

class User {
    String username;
}