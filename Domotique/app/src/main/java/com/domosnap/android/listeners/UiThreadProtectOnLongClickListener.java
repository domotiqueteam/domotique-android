package com.domosnap.android.listeners;

import android.view.View;

import com.domosnap.android.application.DomotiqueApplication;

import java.util.concurrent.ExecutorService;

public abstract class UiThreadProtectOnLongClickListener implements View.OnLongClickListener{

    private ExecutorService frontExecutorService;

    public UiThreadProtectOnLongClickListener(){
        frontExecutorService = DomotiqueApplication.getInstance().getServiceManager().getFrontExecutorService();
    }



    @Override
    final public boolean onLongClick(final View view) {
        frontExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                UiThreadProtectOnLongClickListener.this._onLongClick(view);
            }
        });
        return true;
    }

    public abstract void _onLongClick(View view);
}
