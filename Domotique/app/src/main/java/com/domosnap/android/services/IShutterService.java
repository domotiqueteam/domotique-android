package com.domosnap.android.services;

import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.where.Where;

/**
 * Created by Mikael on 31.01.2017.
 */

public interface IShutterService {

    Shutter findBy(Where where);
}
