package com.domosnap.android.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.domosnap.android.R;

/**
 * Created by Mikael on 20.03.2017.
 */

public class ChangeLightLabelDialog extends AbstractDialog {


    public ChangeLightLabelDialog(@NonNull Context context) {
        super(context, R.layout.dialog_light_label);

        Button cancelButton = (Button)this.findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeLightLabelDialog.this.dismiss();
            }
        });

    }


}
