package com.domosnap.android.services;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;


@RunWith(JUnit4.class)
public class HueUserFinderTest {

    @Test
    public void testParseSuccessMessage() {

        String successMessage
                = "[ {\"success\": { \"username\": \"JoFu665Wu4--yCocSA3H1iUILO3cLm7dXTDaDj--\"  }}]";

        Gson gson = new Gson();

        Success[] successes = gson.fromJson(successMessage, Success[].class);

        assertEquals("JoFu665Wu4--yCocSA3H1iUILO3cLm7dXTDaDj--", successes[0].success.username);

    }

}
