package com.domosnap.android.model;

import com.domosnap.engine.controller.Controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by Mikael on 03/05/2016.
 */
@RunWith(JUnit4.class)
public class HouseTest {

    @Test
    public void testNoDoublonAdd(){
        //Given
        House house = new House();
        Controller controller = newController();

        //When
        house.addController(controller);
        house.addController(controller);

        //Then
        Assert.assertEquals(1,house.getControllers().size());
        Assert.assertEquals(controller,house.getControllers().iterator().next());


    }

    private Controller newController() {
//        Controller controller = Mockito.mock(Controller.class);
//        Mockito.when(controller.getWhere()).thenReturn(Mockito.mock(Where.class));
        return null;
    }
}
