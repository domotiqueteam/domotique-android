package com.domosnap.android.model;

import com.domosnap.engine.controller.where.Where;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by Mikael on 04.12.2016.
 */
@RunWith(JUnit4.class)
public class WhereConverterTest {

    private House.WhereConverter whereConverter = new House.WhereConverter();

    @Test
    public void testToStringFormat() {
        Where where = new Where("15", "17");

        String idAsString = whereConverter.toStringFormat(where);

        Assert.assertEquals("15|17", idAsString);
    }

    @Test
    public void testConvert() {
        String idAsString = "15|17";
        Where where = whereConverter.convert(idAsString);

        Assert.assertEquals("15", where.getFrom());
        Assert.assertEquals("17", where.getTo());
    }

    @Test
    public void simpleTestEquals() {
        Where where1 = new Where("1", "3");
        Where where2 = new Where("1", "3");

        Assert.assertEquals(where1, where2);
        Assert.assertEquals(where2, where1);

        Assert.assertEquals(where1.hashCode(), where2.hashCode());
    }

}
